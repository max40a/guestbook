<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>View Page</title>
</head>
<body>
<center>
    <h2>Book reviews</h2>
    <hr>
    <table border="1">
        <tr>
            <th>Date</th>
            <th>Comment</th>
            <th>Evaluation</th>
        </tr>
        <c:forEach items="${content}" var="content">
            <tr>
                <td><c:out value="${content.date}"/></td>
                <td><c:out value="${content.name}"/></td>
                <td><c:out value="${content.evaluation}"/></td>
            </tr>
        </c:forEach>
    </table>
</center>
</body>
</html>

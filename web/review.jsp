<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Review Page</title>
</head>
<body>
<center><h2>Please leave your evaluation of the service</h2></center>
<hr>
<form method="get" action="/review">
    <table>
        <tr>
            <td>Name :</td>
            <td><input type="text" name="name"></td>
        </tr>
        <tr>
            <td>Comment :</td>
            <td><textarea cols="40" rows="4" name="comment"></textarea></td>
        </tr>
        <tr>
            <td>Evaluation :</td>
            <td>
                <select name="item">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
            </td>
        </tr>
        <tr>
            <td></td>
            <td align="right"><input type="submit" value="Estimate"></td>
        </tr>
    </table>
</form>
</body>
</html>
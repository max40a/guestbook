package servlets;


import dao.EntityDao;
import dao.mysqlimpl.StorageException;
import entity.Review;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

@WebServlet("/view")
public class ViewReviewServlet extends HttpServlet {

    private EntityDao storageEntity;

    @Override
    public void init() throws ServletException {
        ServletContext context = getServletContext();
        storageEntity = (EntityDao) context.getAttribute("storageEntity");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            List<Review> reviewList = storageEntity.list(Review.class);

            Collections.sort(reviewList, (r1, r2) ->
                    r1.getDate().isAfter(r2.getDate()) ? -1 : r1.getDate().isBefore(r2.getDate()) ? 1 : 0);

            req.setAttribute("content", reviewList);
            req.getRequestDispatcher("viewReview.jsp").forward(req, resp);
        } catch (StorageException e) {
            e.printStackTrace();
            throw new ServletException("Error !!!");
        }
    }
}
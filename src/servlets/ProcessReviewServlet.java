package servlets;


import dao.EntityDao;
import entity.Review;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;

@WebServlet("/review")
public class ProcessReviewServlet extends HttpServlet {

    private EntityDao storageEntity;

    @Override
    public void init() throws ServletException {
        ServletContext context = getServletContext();
        storageEntity = (EntityDao) context.getAttribute("storageEntity");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        String comment = req.getParameter("comment");
        Integer item = Integer.valueOf(req.getParameter("item"));

        Review review = new Review();
        review.setName(name);
        review.setComment(comment);
        review.setDate(LocalDate.now());
        review.setEvaluation(item);

        try {
            storageEntity.save(review);
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServletException("Error!!!", e);
        }

        resp.sendRedirect("/view");
    }
}

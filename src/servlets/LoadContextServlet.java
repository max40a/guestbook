package servlets;


import dao.EntityDao;
import sources.DataSource;
import sources.mysqldatasource.MySqlDataSource;
import dao.mysqlimpl.EntityDaoImpl;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebListener;

@WebListener
public class LoadContextServlet implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ServletContext servletContext = servletContextEvent.getServletContext();
        DataSource source;
        try {
            source = new MySqlDataSource();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();// j4log
            throw new RuntimeException("LoadContextException", e);
        }
        EntityDao storage = new EntityDaoImpl(source);
        servletContext.setAttribute("storageEntity", storage);
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
    }
}
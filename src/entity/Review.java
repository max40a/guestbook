package entity;

import java.time.LocalDate;

public class Review extends Entity {

    private String name;
    private String comment;
    private Integer evaluation;
    @DateMarker
    private LocalDate date;

    public void setName(String name) {
        this.name = name;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setEvaluation(Integer evaluation) {
        this.evaluation = evaluation;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public String getComment() {
        return comment;
    }

    public Integer getEvaluation() {
        return evaluation;
    }

    public LocalDate getDate() {
        return date;
    }
}

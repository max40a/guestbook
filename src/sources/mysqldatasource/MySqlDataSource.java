package sources.mysqldatasource;

import sources.DataSource;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class MySqlDataSource implements DataSource {

    private String jdbcDriver;
    private String url;
    private String username;
    private String password;

    public MySqlDataSource() throws ClassNotFoundException {
        getDataBaseParameters();
        Class.forName(jdbcDriver);
    }

    @Override
    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(url, username, password);
    }

    private void getDataBaseParameters() {
        Properties properties = new Properties();
        try (InputStream in = Files.newInputStream(Paths.get("C:\\Users\\Retro\\IdeaProjects\\BookReviews\\resources\\mysql.database.properties"))) {
            properties.load(in);
        } catch (IOException e) {
            e.printStackTrace();
        }

        jdbcDriver = properties.getProperty("jdbc_driver");
        url = properties.getProperty("db_url");
        username = properties.getProperty("username");
        password = properties.getProperty("password");
    }
}
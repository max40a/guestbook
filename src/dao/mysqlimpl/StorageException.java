package dao.mysqlimpl;

public class StorageException extends Exception {

    public StorageException(Throwable cause) {
        super(cause);
    }
}
